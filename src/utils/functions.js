export const compareByAlph = (a, b) => {
    if (a > b) { return -1; } if (a < b) { return 1; } return 0;
};

export function removeSpecialChars(value) {
    let fixedValue = value.toLowerCase();
    fixedValue = fixedValue.replace(/[àáâãäå]/g, 'a');
    fixedValue = fixedValue.replace(/[éèêë]/g, 'e');
    fixedValue = fixedValue.replace(/[íìî]/g, 'i');
    fixedValue = fixedValue.replace(/[óòôõ]/g, 'o');
    fixedValue = fixedValue.replace(/[úùû]/g, 'u');
    fixedValue = fixedValue.replace(/[ -]/g, '_');
    fixedValue = fixedValue.replace(/[ç]/g, 'c');
    return fixedValue;
}

export const isOpen = test => {
    const now = new Date()
    return test.open && new Date(test.open_sub) < now && new Date(test.close_sub) > now;
};