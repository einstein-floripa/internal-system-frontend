export const TIPO_USUARIO = {
  ALUNO: 1,
  ORGANIZACAO: 2,
  DOCENTE: 3
};

export const TIPO_OCUPACAO = {
  STU: 1,
  DOC: 2,
  BAN: 4,
  EMB: 14,
  GOL: 5,
  HOG: 6,
  HOL: 7,
  INT: 8,
  MAT: 9,
  TRI: 10
};
