import React from "react";
import {
  HashRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import {
  Login,
  HomeSistemaInterno,
  RecuperarSenha,
  NotFoundPage,
  Chamada
} from "../Pages";
import PrivateRouter from "./private";
import AlunosRouter from "./alunos";
import OrganizacaoRouter from "./organizacao";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/login/" component={Login} />
        <Route exact path="/recovery/:token" component={RecuperarSenha} />
        <AlunosRouter path="/home-alunos/" component={HomeSistemaInterno} />
        <OrganizacaoRouter
          path="/home-organizacao/"
          component={HomeSistemaInterno}
        />
        <PrivateRouter path="/home-matrix/" component={HomeSistemaInterno} />
        <OrganizacaoRouter path="/chamada" component={Chamada} />
        <Redirect from="/" to="/login" />
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </Router>
  );
};

export default Routes;
