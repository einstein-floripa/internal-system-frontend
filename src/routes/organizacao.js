import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { TIPO_USUARIO } from "../utils/constantes";

const hasToken = () => !!localStorage.getItem("tokeninstein");

const OrganizacaoRouter = ({ component: Component, render, type, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      if (
        hasToken() && type === TIPO_USUARIO.ORGANIZACAO
      ) {
        return <Component {...props} />;
      }
      return <Redirect to="/login" />;
    }}
  />
);

const mapStateToProps = state => ({
  type: state.user.type
});

export default connect(mapStateToProps)(OrganizacaoRouter);
