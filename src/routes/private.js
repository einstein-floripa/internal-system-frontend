import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { TIPO_OCUPACAO } from "../utils/constantes";

const hasToken = () => !!localStorage.getItem("tokeninstein");

const PrivateRoute = ({ component: Component, render, ocupation, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      if (hasToken() && ocupation === TIPO_OCUPACAO.MAT) {
        return <Component {...props} />;
      }
      return <Redirect to="/login" />;
    }}
  />
);

const mapStateToProps = state => ({
  ocupation: state.user.ocupation
});

export default connect(mapStateToProps)(PrivateRoute);
