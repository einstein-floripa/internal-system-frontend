import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { TIPO_USUARIO, TIPO_OCUPACAO } from "../utils/constantes";

const hasToken = () => !!localStorage.getItem("tokeninstein");

const AlunosRouter = ({ component: Component, render, type, ocupation, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      if (
        hasToken() &&
        (type === TIPO_USUARIO.ALUNO || ocupation === TIPO_OCUPACAO.MAT)
      ) {
        return <Component {...props} />;
      }
      return <Redirect to="/login" />;
    }}
  />
);

const mapStateToProps = state => ({
  type: state.user.type,
  ocupation: state.user.ocupation
});

export default connect(mapStateToProps)(AlunosRouter);
