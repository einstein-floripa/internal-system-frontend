import React from 'react'
import { Link } from 'react-router-dom'
import { einstein_to_text } from '../assets/images';
import colors from '../assets/theme/colors';
import styled from "styled-components";

const Div = styled.div`
    p{
        margin-left: 50%;
        margin-right: 20px;
    }
    h1{
        color: ${colors.primary_blue};
        font-size: 4em; 
        text-align: center;
    }
    h2{
        color: ${colors.secondary_blue};
        text-align: right;
    }
    .input-group-btn{
        color: ${colors.secondary_purple};
    }
    height: 100vh;
    background-image: url(${einstein_to_text});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
`

export default () => (
    <Div>
        <p>
            <h1>
                E = mc²
            </h1>
            <h1>
                404 = Não Encontrado
            </h1>

            <h2>
                Desculpe-me, mas a página solicitada não está disponível no momento.
            <br />
                Ela pode estar em construção, sob revisão, possivelmente removida ou não existe.
            <br />
                Por favor, tente novamente mais tarde.
            <br />
                A qualquer momento.
            <br />
                Quando quiser.
            </h2>

            <h1>
                O TEMPO É RELATIVO
            </h1>

            <Link to="/login"><h2 className="input-group-btn">Clique aqui para retornar ao login</h2></Link>

        </p>
    </Div >
)