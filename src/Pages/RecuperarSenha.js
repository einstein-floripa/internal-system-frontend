import React from "react";
import { Form, Input, Button, Popconfirm } from "antd";
import { recoveryPassword } from "../Providers/LoginProvider";
import { logo_vertical } from "../assets/images";

class LoginPage extends React.PureComponent {
  state = {
    confirmDirty: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("Received values of form: ", values);
      if (!err) {
        recoveryPassword({
          token: this.props.match.params.token,
          password: values.password
        }).then(resp => {
          console.log(resp);
          window.location = "/";
        });
      }
    });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("As senhas digitadas não são iguais!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div style={{ textAlign: "center" }}>
        <div style={{ display: "inline-block" }}>
          <Form onSubmit={this.handleSubmit} style={{ maxWidth: "300px" }}>
            <div>
              <img
                width="100%"
                alt="Einsten Floripa Vestibulares"
                src={logo_vertical}
                style={{ padding: 20 }}
              />
            </div>
            <Form.Item label="Senha" hasFeedback>
              {getFieldDecorator("password", {
                rules: [
                  {
                    required: true,
                    message: "Por favor digite sua nova senha!"
                  },
                  {
                    validator: this.validateToNextPassword
                  }
                ]
              })(<Input.Password />)}
            </Form.Item>
            <Form.Item label="Confirme a senha" hasFeedback>
              {getFieldDecorator("confirm", {
                rules: [
                  {
                    required: true,
                    message: "Por favor confirme sua senha!"
                  },
                  {
                    validator: this.compareToFirstPassword
                  }
                ]
              })(<Input.Password onBlur={this.handleConfirmBlur} />)}
            </Form.Item>
            <Form.Item>
              <Popconfirm
                title="Confirmar alteração da senha?"
                onConfirm={this.handleSubmit}
                okText="Sim"
                cancelText="Não"
                placement="bottom"
              >
                <Button
                  type="danger"
                  style={{
                    width: "100%"
                  }}
                >
                  {"ALTERAR SENHA"}
                </Button>
              </Popconfirm>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default Form.create({ name: "login" })(LoginPage);
