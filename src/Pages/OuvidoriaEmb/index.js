import React, { useEffect, useState, useCallback } from "react"
import { OuvidoriaProvider } from "../../Providers"
import { Tabs, List, Descriptions, Checkbox, Spin, notification, Badge } from 'antd'
import moment from "moment";

const ListaOcorrencias = ({ ocorrencias, changeSolved, loadingCheckbox }) => {
    return (
        <List style={{ padding: '0 10px' }}>
            {ocorrencias.map(occ => console.log(occ) || (
                <List.Item>
                    <Descriptions
                        title={
                            <> {occ.title}
                                {loadingCheckbox === occ.id ?  <> <Spin /> Resolvida </> : (
                                    <Checkbox
                                        disabled={loadingCheckbox}
                                        style={{ marginLeft: 10 }}
                                        checked={occ.solved}
                                        onChange={() => {
                                            changeSolved(occ.id);
                                        }}
                                    >
                                        Resolvida
                                    </Checkbox>
                                )}
                                
                        </>
                        }
                    >
                        <Descriptions.Item label="Nome">
                            {occ.student_name || "Anônimo"}
                        </Descriptions.Item>
                        <Descriptions.Item label="Email">
                            {occ.student_mail || "Anônimo"}
                        </Descriptions.Item>
                        <Descriptions.Item label="Telefone">
                            {occ.student_phone || "Anônimo"}
                        </Descriptions.Item>
                        <Descriptions.Item label="Tipo">
                            {occ.type}
                        </Descriptions.Item>
                        <Descriptions.Item label="Criada em">
                            {moment(occ.creation).format("DD/MM/YYYY")}
                        </Descriptions.Item>
                        <Descriptions.Item label="Atualizada em">
                            {moment(occ.modified).format("DD/MM/YYYY")}
                        </Descriptions.Item>
                        <Descriptions.Item label="Mensagem">
                            {occ.content}
                        </Descriptions.Item>
                    </Descriptions>
                </List.Item>
            ))}
        </List>)
}

const styleTab = {
    overflow: "auto",
    height: "calc(100vh - 250px)"
};

export default () => {
    const [resolvidas, setResolvidas] = useState([]);
    const [naoResolvidas, setNaoResolvidas] = useState([]);
    const [loading, setLoading] = useState(true);
    const [loadingCheckbox, setLoadingCheckbox] = useState()

    useEffect(() => {
        Promise.all([OuvidoriaProvider.fetchSolveds(), OuvidoriaProvider.fetchUnsolveds()]).then(([res, nres]) => {
            setResolvidas(res);
            setNaoResolvidas(nres);
            setLoading(false);
        });
    }, []);

    const changeSolved = useCallback((id) => {
        setLoadingCheckbox(id);
        OuvidoriaProvider.toggleResolved(id).then(resp => {
            console.log(resp);
            setLoading(true);
            Promise.all([OuvidoriaProvider.fetchSolveds(), OuvidoriaProvider.fetchUnsolveds()]).then(([res, nres]) => {
                setResolvidas(res);
                setNaoResolvidas(nres);
                setLoadingCheckbox(undefined)
                setLoading(false);
            });
        })
    }, [])

    return (
        <Tabs>
            <Tabs.TabPane
                tab={<> Não Resolvidas {loading ? <Spin /> : <Badge count={naoResolvidas.length} showZero style={{ backgroundColor: naoResolvidas.length > 0 ? "red" : "green" }} />}  </>}
                key="nao_resolvidas"
                style={styleTab}>
                <ListaOcorrencias loadingCheckbox={loadingCheckbox} ocorrencias={naoResolvidas} changeSolved={changeSolved} />
            </Tabs.TabPane>
            <Tabs.TabPane
                tab={<> Resolvidas {loading ? <Spin /> : <Badge count={resolvidas.length} showZero style={{ backgroundColor: "green" }} />}  </>}
                key="resolvidas"
                style={styleTab}>
                <ListaOcorrencias loadingCheckbox={loadingCheckbox} ocorrencias={resolvidas} changeSolved={changeSolved} />
            </Tabs.TabPane>
        </Tabs >
    )
}