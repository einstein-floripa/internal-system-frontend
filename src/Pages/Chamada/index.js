import React from "react";
import { Tabs } from "antd";
import CardScanner from "./CardScanner";
import CardManual from "./CardManual";
import { Redirect } from "react-router-dom";

export default class Chamada extends React.Component {
  state = { activePage: "1" };

  render() {
    const { activePage } = this.state;
    return (
      <Tabs
        defaultActiveKey={activePage}
        onChange={active => this.setState({ activePage: active })}
      >
        <Tabs.TabPane tab="Automático" key="1">
          <CardScanner active={activePage === "1"} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Manual" key="2">
          <CardManual active={activePage === "2"} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Retornar" key="3">
          <Redirect to="/home-organizacao"></Redirect>
        </Tabs.TabPane>
      </Tabs>
    );
  }
}
