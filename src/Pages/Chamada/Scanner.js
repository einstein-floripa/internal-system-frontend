import React, { Component } from "react";
import Quagga from "quagga";
import styled from "styled-components";
import windowSize from "react-window-size";
import { isMobile } from "react-device-detect";

class Scanner extends Component {
  constructor(props) {
    super(props);
    this._onDetected = this._onDetected.bind(this);
    this.state = { capabilities: {}, currentZoom: null };
  }

  Div = styled.div`
    canvas.drawing,
    canvas.drawingBuffer {
      position: absolute;
      top: 80;
      left: 0;
      margin: 0 ${(this.props.windowWidth - this.props.windowWidth / 1.5) / 2}px;
    }
  `;

  componentDidMount() {
    this.getCapabilities();
    Quagga.init(
      {
        inputStream: {
          type: "LiveStream",
          constraints: {
            width: isMobile
              ? this.props.windowHeight / 4
              : this.props.windowWidth / 1.5,
            height: isMobile
              ? this.props.windowWidth / 1.5
              : this.props.windowHeight / 4,
            facingMode: "environment" // environment or user
          }
        },
        locator: {
          patchSize: "large",
          halfSample: true
        },
        numOfWorkers: 0,
        decoder: {
          readers: [
            "code_128_reader"
            //'i2of5_reader',
            //'2of5_reader'
          ]
        },
        locate: true //false,
      },
      err => {
        if (err) {
          return console.log("err: ", err);
        }
        Quagga.start();
        console.log("start");
      }
    );
    Quagga.onDetected(this._onDetected);

    Quagga.onProcessed(result => {
      var drawingCtx = Quagga.canvas.ctx.overlay,
        drawingCanvas = Quagga.canvas.dom.overlay;

      if (result && this.props.scanning) {
        if (result.boxes) {
          drawingCtx.clearRect(
            0,
            0,
            parseInt(drawingCanvas.getAttribute("width")),
            parseInt(drawingCanvas.getAttribute("height"))
          );
          result.boxes
            .filter(function(box) {
              return box !== result.box;
            })
            .forEach(box => {
              Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, {
                color: "green",
                lineWidth: 2
              });
            });
        }

        if (result.box) {
          Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, {
            color: "#00F",
            lineWidth: 2
          });
        }

        if (result.codeResult && result.codeResult.code) {
          Quagga.ImageDebug.drawPath(
            result.line,
            { x: "x", y: "y" },
            drawingCtx,
            { color: "red", lineWidth: 3 }
          );
        }
      } else {
        drawingCtx.clearRect(
          0,
          0,
          parseInt(drawingCanvas.getAttribute("width")),
          parseInt(drawingCanvas.getAttribute("height"))
        );
      }
    });
  }

  componentWillUnmount() {
    Quagga.offDetected(this._onDetected);
  }

  _onDetected(result) {
    //console.log('result: ', result);
    if (this.props.scanning) {
      this.props.onDetected(result);
    }
  }

  getCapabilities() {
    navigator.mediaDevices
      .getUserMedia({
        video: {
          facingMode: "environment"
        }
      })
      .then(stream => {
        const video = document.querySelector("video");
        video.srcObject = stream;

        // get the active track of the stream
        const track = stream.getVideoTracks()[0];

        video.addEventListener("loadedmetadata", e => {
          window.setTimeout(
            () => onCapabilitiesReady(track.getCapabilities()),
            500
          );
        });

        const onCapabilitiesReady = capabilities => {
          if (capabilities.zoom) {
            this.setState(prevState => ({
              capabilities: { ...prevState.capabilities, zoom: true },
              currentZoom: capabilities.zoom.min
            }));
            track
              .applyConstraints({
                advanced: [{ zoom: capabilities.zoom.min }]
              })
              .catch(e => console.log(e));
          }
          if (capabilities.torch) {
            this.setState(prevState => ({
              capabilities: { ...prevState.capabilities, torch: true }
            }));
            track
              .applyConstraints({
                advanced: [{ torch: this.props.torch }]
              })
              .catch(e => console.log(e));
          }
        };
      })
      .catch(err => console.error("getUserMedia() failed: ", err));
  }

  componentWillReceiveProps(newProps) {
    //alterar para component did update
    const { capabilities } = this.state;
    if (newProps.torch !== this.props.torch && capabilities.torch) {
      Quagga.CameraAccess.getActiveTrack().applyConstraints({
        advanced: [{ torch: newProps.torch }]
      });
    }
    console.log(newProps);
    if (newProps.zoom && capabilities.zoom) {
      const params = Quagga.CameraAccess.getActiveTrack().getCapabilities()
        .zoom;

      if (newProps.zoom === "in") {
        if (this.state.currentZoom < params.max)
          this.setState(
            prevState => ({ currentZoom: prevState.currentZoom + 1 }),
            () => {
              Quagga.CameraAccess.getActiveTrack().applyConstraints({
                advanced: [{ zoom: this.state.currentZoom }]
              });
            }
          );
      }
      if (newProps.zoom === "out") {
        if (this.state.currentZoom > params.min)
          this.setState(
            prevState => ({ currentZoom: prevState.currentZoom - 1 }),
            () => {
              Quagga.CameraAccess.getActiveTrack().applyConstraints({
                advanced: [{ zoom: this.state.currentZoom }]
              });
            }
          );
      }
    }
  }

  render() {
    return (
      <this.Div
        style={{ textAlign: "center" }}
        id="interactive"
        className="viewport"
      />
    );
  }
}

export default windowSize(Scanner);
