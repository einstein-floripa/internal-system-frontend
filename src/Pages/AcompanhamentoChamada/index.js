import React, { useState, useCallback } from "react";
import moment from "moment";
import { Table, DatePicker, Checkbox, Input } from "antd";
import { ChamadaProvider } from "../../Providers";
import { compareByAlph, removeSpecialChars } from "../../utils/functions";


const columns = (first_register, destacar) => {
    const datas = Object.keys(first_register).filter(key => key !== "name" && key !== "problem" && key !== "matricula" && key !== "attendance_pctg")

    return [
        {
            dataIndex: 'name',
            title: "Nome",
            fixed: true,
            width: 120,
            sorter: (a, b) => { return compareByAlph(removeSpecialChars(b.name), removeSpecialChars(a.name)) },
            render: (n, r) => (
                {
                    props: {
                        style: { background: destacar ? (r.problem ? "#FFADAD" : "#ADFFAD") : "" },
                    },
                    children:
                        (<div> <strong>{n}</strong> </div>),
                })
        },
        {
            dataIndex: 'matricula',
            title: "Matrícula",
            width: 60,
            sorter: (a, b) => { return a.matricula - b.matricula },
            render: (n, r) => (
                {
                    props: {
                        style: { background: destacar ? (r.problem ? "#FFADAD" : "#ADFFAD") : "" },
                    },
                    children:
                        (<div> <strong>{n}</strong> </div>),
                })
        },
        ...datas.map(key => ({
            dataIndex: key,
            title: key,
            width: 40,
            render: (n, r) => (
                {
                    props: {
                        style: { background: destacar ? (r.problem ? "#FFADAD" : "#ADFFAD") : "" },
                    },
                    children:
                        (<div> <strong>{n}</strong> </div>),
                })
        })),
        {
            dataIndex: 'attendance_pctg',
            title: "Porcentagem",
            width: 60,
            sorter: (a, b) => { return a.attendance_pctg - b.attendance_pctg },
            render: (n, r) => (
                {
                    props: {
                        style: { background: destacar ? (r.problem ? "#FFADAD" : "#ADFFAD") : "" },
                    },
                    children:
                        (<div> <strong>{`${Math.round(n * 10000) / 100}%`}</strong> </div>),
                })
        },
        {
            dataIndex: 'problem',
            title: "Problema",
            fixed: "right",
            sorter: (a, b) => { return b.problem - a.problem },
            render: (n, r) => (
                {
                    props: {
                        style: { background: destacar ? (r.problem ? "#FFADAD" : "#ADFFAD") : "" },
                    },
                    children:
                        (<div> <strong>{n === true ? (
                            <span style={{ color: "red" }}>SIM</span>
                        ) : (
                                <span>NÃO</span>
                            )}</strong> </div>),
                })
        }
    ]
};

function disabledDate(current) {
    return current > moment().endOf("month");
}

export default () => {
    const [dadosChamada, setDadosChamada] = useState([]);
    const [loadingDados, setLoadingDados] = useState(false);
    const [destacarProblemas, setDestacarProblemas] = useState(false);
    const [filtroAluno, setFiltroAluno] = useState("");

    const handleChange = useCallback(month => {
        setLoadingDados(true);
        ChamadaProvider.fetchDadosChamada(month).then(resp => {
            setDadosChamada(
                Object.keys(resp).map(matricula => ({ matricula, ...resp[matricula] }))
            );
            setLoadingDados(false);
        });
    }, []);

    return (
        <div>
            <div>
                <DatePicker.MonthPicker
                    disabledDate={disabledDate}
                    placeholder="Selecione o mês"
                    style={{ margin: 10 }}
                    format="MM-YYYY"
                    onChange={change => handleChange(change.format("MM"))}
                />
                <Checkbox value={destacarProblemas} onChange={e => setDestacarProblemas(e.target.checked)}>Destacar Problemas</Checkbox>
                <Input.Search placeholder="Pesquisa por Aluno" onSearch={setFiltroAluno} style={{ width: 200, float: "right", margin: 10 }} />
            </div>
            <div style={{ overflow: "auto", height: "calc(100vh - 250px)" }}>
                <Table
                    pagination={false}
                    loading={loadingDados}
                    columns={dadosChamada.length > 0 ? columns(dadosChamada[0], destacarProblemas) : []}
                    dataSource={dadosChamada.filter(r => removeSpecialChars(r.name).toLowerCase().includes(filtroAluno.toLowerCase()))}
                    rowKey={'matricula'}
                    scroll={{ x: true }}
                    onRow={(record, rowIndex) => {
                        return {
                            onClick: () => console.log("Clicked: ", record)
                        };
                    }}
                />
            </div>
        </div>
    );
};
