import React, { useEffect, useState, useCallback, useMemo } from "react";
import { Table, Select, Checkbox, Button, notification } from "antd";
import { HorariosProvider } from "../Providers";
import { connect } from "react-redux";
import { TIPO_OCUPACAO } from "../utils/constantes";

const rawColumns = (editMode, handleChangeDisciplina, disciplinas) => [
  {
    width: 80,
    fixed: true,
    title: "Horário",
    dataIndex: "time",
    render: text => ({
      props: {
        style: { background: "#f3f3f3" }
      },
      children: <div>{text}</div>
    })
  },
  {
    title: "Segunda",
    dataIndex: "monday",
    render: (text, record) => ({
      props: {
        style: { background: "#ffe8e8" }
      },
      children: editMode ? (
        <Select
          defaultValue={text}
          style={{ width: 130 }}
          mode="multiple"
          onChange={v => handleChangeDisciplina(record.time, "monday", v)}
        >
          {disciplinas.map(k => (
            <Select.Option value={k}>{k}</Select.Option>
          ))}
        </Select>
      ) : (
        <div>
          {text.map(t => (
            <>
              <span>{t}</span> <br />
            </>
          ))}
        </div>
      )
    })
  },
  {
    title: "Terça",
    dataIndex: "tuesday",
    render: (text, record) => ({
      props: {
        style: { background: "#fffee8" }
      },
      children: editMode ? (
        <Select
          defaultValue={text}
          style={{ width: 130 }}
          mode="multiple"
          onChange={v => handleChangeDisciplina(record.time, "tuesday", v)}
        >
          {disciplinas.map(k => (
            <Select.Option value={k}>{k}</Select.Option>
          ))}
        </Select>
      ) : (
        <div>
          {text.map(t => (
            <>
              <span>{t}</span> <br />
            </>
          ))}
        </div>
      )
    })
  },
  {
    title: "Quarta",
    dataIndex: "wednesday",
    render: (text, record) => ({
      props: {
        style: { background: "#efffe8" }
      },
      children: editMode ? (
        <Select
          defaultValue={text}
          style={{ width: 130 }}
          mode="multiple"
          onChange={v => handleChangeDisciplina(record.time, "wednesday", v)}
        >
          {disciplinas.map(k => (
            <Select.Option value={k}>{k}</Select.Option>
          ))}
        </Select>
      ) : (
        <div>
          {text.map(t => (
            <>
              <span>{t}</span> <br />
            </>
          ))}
        </div>
      )
    })
  },
  {
    title: "Quinta",
    dataIndex: "thursday",
    render: (text, record) => ({
      props: {
        style: { background: "#e8edff" }
      },
      children: editMode ? (
        <Select
          defaultValue={text}
          style={{ width: 130 }}
          mode="multiple"
          onChange={v => handleChangeDisciplina(record.time, "thursday", v)}
        >
          {disciplinas.map(k => (
            <Select.Option value={k}>{k}</Select.Option>
          ))}
        </Select>
      ) : (
        <div>
          {text.map(t => (
            <>
              <span>{t}</span> <br />
            </>
          ))}
        </div>
      )
    })
  },
  {
    title: "Sexta",
    dataIndex: "friday",
    render: (text, record) => ({
      props: {
        style: { background: "#fce8ff" }
      },
      children: editMode ? (
        <Select
          defaultValue={text}
          style={{ width: 130 }}
          mode="multiple"
          onChange={v => handleChangeDisciplina(record.time, "friday", v)}
        >
          {disciplinas.map(k => (
            <Select.Option value={k}>{k}</Select.Option>
          ))}
        </Select>
      ) : (
        <div>
          {text.map(t => (
            <>
              <span>{t}</span> <br />
            </>
          ))}
        </div>
      )
    })
  }
];

const HorariosAulas = ({ ocupation }) => {
  const [turmaSelecionada, setTurmaSelecionada] = useState(null);
  const [horarios, setHorarios] = useState([]);
  const [turmas, setTurmas] = useState([]);
  const [loading, setLoading] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [disciplinas, setDisciplinas] = useState([]);

  const changeTurmaSelecionada = useCallback(
    v => {
      setTurmaSelecionada(v);
      setLoading(true);
      HorariosProvider.fetchContatos(v).then(horario => {
        setHorarios(horario);
        setLoading(false);
      });
    },
    [setLoading, setTurmaSelecionada, setHorarios]
  );

  const handleChangeEditMode = useCallback(e => {
    setEditMode(e.target.checked);
  }, []);

  useEffect(() => {
    Promise.all([
      HorariosProvider.fetchTurmas(),
      HorariosProvider.fetchDisciplinas()
    ]).then(([t, d]) => {
      setTurmas(t);
      setDisciplinas(d);
      if (t.length > 0) {
        setTurmaSelecionada(t[0].label);
        HorariosProvider.fetchTurmas(t[0].label).then(horario => {
          setHorarios(horario);
          setLoading(false);
        });
      }
    });
  }, []);

  const handleChangeDisciplina = useCallback(
    (time, column, value) => {
      const novoHorario = horarios.map(h =>
        h.time === time ? { ...h, [column]: value } : h
      );
      setHorarios(novoHorario);
    },
    [horarios]
  );

  const columns = useMemo(() => {
    return rawColumns(editMode, handleChangeDisciplina, disciplinas);
  }, [disciplinas, editMode, handleChangeDisciplina]);

  const handleSubmit = useCallback(() => {
    setLoading(true);
    HorariosProvider.salvarHorarios(turmaSelecionada, horarios).then(resp => {
      if (resp.detail === "class registers modified") {
        notification.success({
          message: "Sucesso",
          description: "Horários alterados"
        });
        HorariosProvider.fetchTurmas(turmaSelecionada).then(horario => {
          setHorarios(horario);
          setEditMode(false);
          setLoading(false);
        });
      } else {
        notification.error({ message: "Erro", description: resp.detail || "Não foi possível salvar a alteração" });
        setLoading(false);
      }
    });
  }, [horarios, turmaSelecionada]);

  return (
    <div>
      <div style={{ padding: 15 }}>
        Turma:
        <Select
          loading={turmas.length === 0}
          onSelect={changeTurmaSelecionada}
          value={turmaSelecionada}
        >
          {turmas.map(turma => (
            <Select.Option key={turma.label} value={turma.label}>
              {`${turma.name} - ${turma.label}`}
            </Select.Option>
          ))}
        </Select>
        {ocupation === TIPO_OCUPACAO.MAT && (
          <Checkbox
            style={{ marginLeft: 15 }}
            onChange={handleChangeEditMode}
            checked={editMode}
            disabled={!turmaSelecionada}
          >
            Edição
          </Checkbox>
        )}
        {editMode && (
          <Button
            onClick={handleSubmit}
            type="primary"
            disabled={!turmaSelecionada}
          >
            Salvar
          </Button>
        )}
      </div>
      <Table
        scroll={{ x: true }}
        bordered
        columns={columns}
        dataSource={horarios}
        loading={loading}
        pagination={false}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  type: state.user.type,
  ocupation: state.user.ocupation
});

export default connect(mapStateToProps)(HorariosAulas);
