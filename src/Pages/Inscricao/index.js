import React, { useCallback } from "react";
import { bindActionCreators } from "redux";
import { Creators as InfoAndSubsActions } from "../../store/ducks/infoAndSubs";
import { connect } from "react-redux";
import { Modal } from "antd";
import { SimuladosProvider } from "../../Providers";
import FormInscricao from "./FormInscricao";

import { isOpen } from "../../utils/functions"
import ListaSimulados from "../../Components/ListaSimulados";

const Inscricao = ({ infoAndSubs, setInfos }) => {
  const fetchInfoAndSubs = useCallback(() => {
    SimuladosProvider.fetchInfosSubs().then(resp => setInfos(resp));
  }, [setInfos]);

  const handleClick = useCallback(
    t => {
      const modal = Modal.confirm();

      if (isOpen(t)) {
        if (!t.user_sub) {
          modal.update({
            maskClosable: true,
            title: `Realizar inscrição no ${t.label}`,
            content: (
              <FormInscricao
                modal={modal}
                infoAndSubs={infoAndSubs}
                test={t}
                fetchInfoAndSubs={fetchInfoAndSubs}
              />
            ),
            okText: "Realizar inscrição",
            okButtonProps: {
              type: "primary",
              htmlType: "submit",
              key: "submit",
              form: "formInscricaoSimulado"
            },
            onOk: close => { },
            cancelText: "Cancelar"
          });
        } else {
          modal.update({
            maskClosable: true,
            title: `Editar inscrição no ${t.label}`,
            content: (
              <FormInscricao
                modal={modal}
                infoAndSubs={infoAndSubs}
                test={t}
                fetchInfoAndSubs={fetchInfoAndSubs}
                edit={true}
              />
            ),
            okText: "Editar inscrição",
            okButtonProps: {
              type: "primary",
              htmlType: "submit",
              key: "submit",
              form: "formInscricaoSimulado"
            },
            onOk: close => { },
            cancelText: "Cancelar"
          });
        }
      } else {
        modal.destroy();
      }
    },
    [fetchInfoAndSubs, infoAndSubs]
  );

  return (
    <div>
      <div>
        <ListaSimulados infoAndSubs={infoAndSubs} onClick={handleClick} />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  infoAndSubs: state.infoAndSubs
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(InfoAndSubsActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inscricao);
