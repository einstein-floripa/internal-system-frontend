import React, { useCallback } from "react";

import { Form, Select, notification, Button, Popconfirm } from "antd";

import { isOpen } from "../../utils/functions";

import { SimuladosProvider } from "../../Providers";

import { compareByAlph } from "../../utils/functions"

export default Form.create()(
  ({
    form: { getFieldDecorator, validateFields },
    infoAndSubs: { courses, langs, quotas },
    test,
    modal,
    fetchInfoAndSubs,
    edit
  }) => {
    const handleSubmit = useCallback(
      e => {
        e.preventDefault();
        validateFields((err, values) => {
          if (!err) {
            modal.destroy();
            if (isOpen(test)) {
              SimuladosProvider.registrarInscricao({
                ...values,
                id_test: test.id
              }).then(({ done }) => {
                if (done) {
                  notification.success({
                    message: "Sucesso",
                    description: "Inscrição realizada"
                  });
                } else {
                  notification.warning({
                    message: "Atenção",
                    description: "Não foi possível realizar a sua inscrição"
                  });
                }
                fetchInfoAndSubs();
              });
            } else
              notification.warn({
                message: "Atenção",
                description: "As inscrições para este simulado estão fechadas!"
              });
          }
        });
      },
      [fetchInfoAndSubs, modal, test, validateFields]
    );

    const handleRemove = useCallback(() => {
      modal.destroy();
      SimuladosProvider.cancelarInscricao(test.id).then(({ done }) => {
        if (done) {
          notification.success({
            message: "Sucesso",
            description: "Inscrição removida"
          });
        } else {
          notification.warning({
            message: "Atenção",
            description: "Não foi possível remover a sua inscrição"
          });
        }
        fetchInfoAndSubs();
      });
    }, [fetchInfoAndSubs, modal, test.id]);

    return (
      <Form id={"formInscricaoSimulado"} onSubmit={handleSubmit}>
        <Form.Item label="Curso">
          {getFieldDecorator("id_course", {
            initialValue: test.user_sub_data && test.user_sub_data.course,
            rules: [
              {
                required: true,
                message: "Por favor escolha uma opção de curso!"
              }
            ]
          })(
            <Select showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {courses.sort((a, b) => compareByAlph(b.label, a.label)).map(course => (
                <Select.Option key={course.id} value={course.id}>
                  {`${course.label}`}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Segunda Lingua">
          {getFieldDecorator("id_lang", {
            initialValue: test.user_sub_data && test.user_sub_data.language,
            rules: [
              {
                required: true,
                message: "Por favor escolha uma opção de segunda língua!"
              }
            ]
          })(
            <Select showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {langs.sort((a, b) => compareByAlph(b.descriptive_text, a.descriptive_text)).map(lang => (
                <Select.Option key={lang.id} value={lang.id}>
                  {lang.descriptive_text}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Cotas">
          {getFieldDecorator("id_quota", {
            initialValue: test.user_sub_data && test.user_sub_data.quota,
            rules: [
              {
                required: true,
                message: "Por favor escolha uma opção de cota!"
              }
            ]
          })(
            <Select showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {quotas.sort((a, b) => compareByAlph(a.descriptive_text, b.descriptive_text)).map(quota => (
                <Select.Option key={quota.id} value={quota.id}>
                  {quota.descriptive_text}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        {edit && (
          <Popconfirm
            title={<strong>Realmente deseja remover sua inscrição?</strong>}
            onConfirm={handleRemove}
            okText="Sim"
            okButtonProps={{ type: "danger" }}
            cancelText="Não"
            cancelButtonProps={{ type: "primary" }}
            placement="bottom"
          >
            <Button type="danger" style={{ width: "100%" }}>
              Remover Inscrição
            </Button>
          </Popconfirm>
        )}
      </Form>
    );
  }
);
