import React, { useEffect, useState } from "react";
import { ContatosProvider } from "../Providers";
import { List, Row, Col, Spin } from "antd";
import { isMobile } from "react-device-detect";
import { compareByAlph, removeSpecialChars } from "../utils/functions";

const cargoDefault = {
  embaixada: "Embaixador(a)",
  hogwarts: "Bruxo(a)",
  docentes: "Docente"
};

const Contatos = ({ activeContent }) => {
  const [contatos, setContatos] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    ContatosProvider.fetchContatos(activeContent.substring(0, 3)).then(resp => {
      setContatos(resp);
      setLoading(false);
    });
  }, [activeContent]);

  return (
    <div>
      {!isMobile ? (
        <div
          style={{
            width: "100%",
            padding: 16,
            borderBottom: "1px solid #ccc"
          }}
        >
          <Row gutter={8}>
            <Col span={8}>
              <strong>Nome</strong>
            </Col>
            <Col span={8}>
              <strong>Contato</strong>
            </Col>
            <Col span={8}>
              {activeContent.substring(0, 3) === "stu" ? (<strong>Turma</strong>) : (<strong>Cargo</strong>)}
            </Col>
          </Row>
        </div>
      ) : (
          <></>
        )}
      <List style={{ overflow: "auto", height: "calc(100vh - 240px)" }}>
        {loading ? (
          <List.Item style={{ justifyContent: "center" }}>
            <Spin />
          </List.Item>
        ) : (
            contatos
              .sort((a, b) => compareByAlph(removeSpecialChars(b.name), removeSpecialChars(a.name)))
              .map(contato => (
                <List.Item>
                  <div style={{ width: "100%", padding: 16 }}>
                    <Row gutter={8}>
                      <Col span={24} lg={8}>
                        {contato.name}
                      </Col>
                      <Col span={24} lg={8}>
                        {contato.contact}
                      </Col>
                      <Col span={24} lg={8}>
                        {contato.cargo || contato.turma || cargoDefault[activeContent]}
                      </Col>
                    </Row>
                  </div>
                </List.Item>
              ))
          )}
      </List>
    </div>
  );
};

export default Contatos;
