import React, { useEffect, useState } from "react";
import { ResourcesProvider } from "../Providers";
import { Card } from "antd";

const Resources = () => {
  const [resources, setResources] = useState([]);

  useEffect(() => {
    ResourcesProvider.fetchResources().then(setResources);
  }, []);

  return (
    <div>
      <div>
        {resources.map(resource => (
          <Card
            key={resource.description}
            onClick={() => window.open(resource.link)}
            hoverable
            title={resource.description}
          >
            Clique aqui
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Resources;
