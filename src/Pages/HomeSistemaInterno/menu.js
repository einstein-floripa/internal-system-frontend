import React from "react";

import { AlunosProvider } from "../../Providers";

import Inscricao from "../Inscricao/index";
import Resources from "../Resources";
import BemVindos from "../BemVindos";
import AcompanhamentoChamada from "../AcompanhamentoChamada";
import Contatos from "../Contatos";
import HorarioAulas from "../HorarioAulas";
import Ouvidoria from "../Ouvidoria";
import OuvidoriaEmb from "../OuvidoriaEmb";
import CadastroSimulado from "../CadastroSimulado";
import Dashboard from "../Dashboard";
import Wiki from "../Wiki";

import { TIPO_USUARIO, TIPO_OCUPACAO } from "../../utils/constantes";

import { Redirect } from "react-router-dom";

export const contentMenu = {
  home: {
    title: "Início",
    icon: "home",
    content: BemVindos,
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    //level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  /*simulados: {
    filhos: ["inscricao", "results", "previous", "cadastro_simulados"],
    title: "Simulados",
    icon: "book",
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  cadastro_simulados: {
    filho: "simulados",
    content: CadastroSimulado,
    title: "Cadastro Simulado",
    icon: "form",
    access: [TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.MAT],
  },
  inscricao: {
    filho: "simulados",
    content: Inscricao,
    title: "Inscrição",
    icon: "edit",
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  results: {
    filho: "simulados",
    title: "Resultados",
    icon: "line-chart",
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  previous: {
    filho: "simulados",
    title: "Provas Anteriores",
    icon: "container",
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },*/
  resources: {
    content: Resources,
    title: "Recursos e Relatórios",
    icon: "hdd",
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },
  atestado: {
    title: "Atestado de Matrícula",
    onClick: () => {
      AlunosProvider.fetchCertificado().then((blob) => {
        // 2. Create blob link to download
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", `atestado.pdf`);
        // 3. Append to html page
        document.body.appendChild(link);
        // 4. Force download
        link.click();
        // 5. Clean up and remove the link
        link.parentNode.removeChild(link);
      });
    },
    icon: "file-text",
    access: [TIPO_USUARIO.ALUNO],
  },
  horario: {
    title: "Horarios das Aulas",
    icon: "schedule",
    content: HorarioAulas,
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },

  //Contatos
  contacts: {
    filhos: ["embaixada", "docentes", "hogwarts", "students", "ouvidoria"],
    title: "Contatos",
    icon: "phone",
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    //level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT, TIPO_OCUPACAO.EMB],
  },
  embaixada: {
    filho: "contacts",
    title: "Embaixada",
    icon: "heart",
    content: Contatos,
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },
  docentes: {
    filho: "contacts",
    title: "Docentes",
    icon: "smile",
    content: Contatos,
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },
  hogwarts: {
    filho: "contacts",
    title: "Hogwarts",
    icon: "user",
    content: Contatos,
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },
  students: {
    filho: "contatcts",
    title: "Alunos",
    icon: "smile",
    content: Contatos,
    access: [TIPO_USUARIO.ORGANIZACAO, TIPO_USUARIO.DOCENTE],
  },
  ouvidoria: {
    filho: "contacts",
    title: "Ouvidoria",
    icon: "bank",
    content: Ouvidoria,
    access: [TIPO_USUARIO.ALUNO, TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  ouvidoria_emb: {
    title: "Mensagens Ouvidoria",
    icon: "bank",
    content: OuvidoriaEmb,
    access: [TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.EMB, TIPO_OCUPACAO.MAT],
  },
  //---

  /*calendar: {
    title: "Calendário",
    icon: "calendar",
    access: [
      TIPO_USUARIO.ALUNO,
      TIPO_USUARIO.ORGANIZACAO,
      TIPO_USUARIO.DOCENTE,
    ],
  },*/

  acompanhamento_chamada: {
    title: "Acompanhamento Chamada",
    icon: "snippets",
    content: AcompanhamentoChamada,
    access: [TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.MAT, TIPO_OCUPACAO.EMB],
  },
  chamada: {
    title: "Chamada",
    icon: "scan",
    content: () => <Redirect to="/chamada" />,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },

  /*wiki: {
    title: "WikiEinstein",
    icon: "dashboard",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },*/

  //Contatos
  wiki: {
    filhos: [
      "w_vale",
      "w_embaixada",
      "w_times",
      "w_hogwarts",
      "w_ministerio",
      "w_capital",
    ],
    title: "WikiEinstein",
    icon: "dashboard",
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_vale: {
    filho: "contacts",
    title: "Vale",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_embaixada: {
    filho: "contacts",
    title: "Embaixada",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_times: {
    filho: "contacts",
    title: "Times",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_hogwarts: {
    filho: "contacts",
    title: "Hogwarts",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_ministerio: {
    filho: "contacts",
    title: "Ministério",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  w_capital: {
    filho: "contacts",
    title: "Capital",
    icon: "smile",
    content: Wiki,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  bitwarden: {
    title: "Bitwarden",
    //icon: "dashboard",
    onClick: () =>
      (window.location.href = "https://vault.bitwarden.com/#/vault"),
    access: [TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  cpanel: {
    title: "Cpanel",
    //icon: "dashboard",
    onClick: () =>
      (window.location.href =
        "https://einsteinfloripa.com.br:2083/cpsess2747225485/frontend/paper_lantern/index.html"),
    access: [TIPO_USUARIO.ORGANIZACAO],
    level: [TIPO_OCUPACAO.STU, TIPO_OCUPACAO.MAT],
  },
  slack: {
    title: "Slack",
    //icon: "dashboard",
    onClick: () =>
      (window.location.href = "https://einsteinfloripa.slack.com/"),
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  trello: {
    title: "Trello",
    //icon: "dashboard",
    onClick: () =>
      (window.location.href = "https://trello.com/einsteinfloripageral/home"),
    access: [TIPO_USUARIO.ORGANIZACAO],
  },
  drive: {
    title: "Google Drive",
    //icon: "dashboard",
    onClick: () =>
      (window.location.href = "https://drive.google.com/drive/shared-drives"),
    access: [TIPO_USUARIO.ORGANIZACAO],
  },

  /*wiki: {
    title: "WikiEinstein",
    icon: "dashboard",
    onClick: () =>
      (window.location.href =
        "https://sites.google.com/einsteinfloripa.com.br/wikieinstein/in%C3%ADcio?read_current=1"),
    access: [TIPO_USUARIO.ORGANIZACAO],
  },*/
  /*dashboard: {
    title: "Dashboard",
    icon: "dashboard",
    content: Dashboard,
    access: [TIPO_USUARIO.ORGANIZACAO],
  },*/
};
