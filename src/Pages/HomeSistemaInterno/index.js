import React from "react";
import {
  icon_for_site,
  einstein_floripa_text,
  spoiler_alert,
} from "../../assets/images";
import { isMobile } from "react-device-detect";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as InfoAndSubsActions } from "../../store/ducks/infoAndSubs";
import { Layout, Menu, Icon, Dropdown, Breadcrumb } from "antd";
import { SimuladosProvider } from "../../Providers";
import { contentMenu } from "./menu";
const { Header, Footer, Sider, Content } = Layout;
const { SubMenu, Item } = Menu;

const menuDropdown = (
  <Menu>
    <Item
      onClick={() => {
        localStorage.removeItem("tokeninstein");
        window.location = "/";
      }}
    >
      Sair
    </Item>
  </Menu>
);

class HomeSistemaInterno extends React.Component {
  state = {
    infoAndSubs: {},
    activeContent: "home",
    collapsed: isMobile ? true : false,
  };

  componentDidMount = () => {
    SimuladosProvider.fetchInfosSubs().then((resp) => {
      this.props.setInfos(resp);
    });
  };

  render() {
    const { activeContent, infoAndSubs } = this.state;
    const { user } = this.props;

    const ActiveContent =
      contentMenu[activeContent] && contentMenu[activeContent].content
        ? contentMenu[activeContent].content
        : () => (
            <div>
              <img
                src={spoiler_alert}
                alt={"Spoiler alert"}
                style={{
                  height: "calc(100% - 55px)",
                  width: "calc(100% - 54px)",
                }}
              />
            </div>
          );

    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={(collapsed) => this.setState({ collapsed })}
        >
          <div className="logo" />
          <Menu
            defaultSelectedKeys={[activeContent]}
            mode="inline"
            onClick={({ key, keyPath, ...rest }) => {
              console.log(key, rest);
              if (!contentMenu[key].onClick) {
                this.setState({ activeContent: key, breadcrumb: keyPath });
              }
            }}
          >
            <div style={{ padding: 8 }}>
              <img
                src={icon_for_site}
                alt=""
                height="44px"
                style={{ marginLeft: this.state.collapsed ? 10 : 8 }}
              />
              {!this.state.collapsed && (
                <img
                  src={einstein_floripa_text}
                  alt="Einstein Floripa"
                  height="56px"
                />
              )}
            </div>

            {Object.keys(contentMenu)
              .filter(
                (k) =>
                  !contentMenu[k].filho &&
                  contentMenu[k].access.includes(user.type) &&
                  (!contentMenu[k].level ||
                    contentMenu[k].level.includes(user.ocupation))
              )
              .map((k) =>
                contentMenu[k].filhos ? (
                  <SubMenu
                    key={k}
                    title={
                      <span>
                        <Icon type={contentMenu[k].icon} />
                        <span>{contentMenu[k].title}</span>
                      </span>
                    }
                  >
                    {contentMenu[k].filhos
                      .filter(
                        (f) =>
                          contentMenu[f].access.includes(user.type) &&
                          (!contentMenu[k].level ||
                            contentMenu[k].level.includes(user.ocupation))
                      )
                      .map((f) => (
                        <Item key={f} onClick={contentMenu[f].onClick}>
                          <Icon type={contentMenu[f].icon} />
                          <span>{contentMenu[f].title}</span>
                        </Item>
                      ))}
                  </SubMenu>
                ) : (
                  <Item key={k} onClick={contentMenu[k].onClick}>
                    <Icon type={contentMenu[k].icon} />
                    <span>{contentMenu[k].title}</span>
                  </Item>
                )
              )}
          </Menu>
        </Sider>
        <Layout>
          <Header
            style={{ background: "#fff", padding: 0, overflow: "hidden" }}
          >
            <strong style={{ fontSize: 24, marginLeft: 10 }}>
              Sistema InternEinstein
            </strong>
            <div style={{ float: "right", marginRight: 10 }}>
              <Dropdown overlay={menuDropdown}>
                <a className="ant-dropdown-link" ref="#">
                  {`Olá, ${user.first_name ? user.first_name : "Aluno"}`}
                </a>
              </Dropdown>
            </div>
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              {this.state.breadcrumb &&
                this.state.breadcrumb
                  .reverse()
                  .map((key) => (
                    <Breadcrumb.Item>{contentMenu[key].title}</Breadcrumb.Item>
                  ))}
            </Breadcrumb>
            <div style={{ background: "#fff" }}>
              <ActiveContent
                infoAndSubs={infoAndSubs}
                activeContent={activeContent}
              />
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Feito com{" "}
            <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" /> pelo
            Vale do Silício
          </Footer>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(InfoAndSubsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeSistemaInterno);
