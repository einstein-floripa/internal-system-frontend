import Login from "./Login";
import NotFoundPage from "./NotFoundPage";
import HomeSistemaInterno from "./HomeSistemaInterno";
import RecuperarSenha from "./RecuperarSenha";
import Chamada from "./Chamada";

export { Login, NotFoundPage, HomeSistemaInterno, RecuperarSenha, Chamada };
