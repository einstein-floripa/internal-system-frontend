import React, { useEffect, useState, useCallback } from "react";
import { OuvidoriaProvider } from "../../Providers";
import {
  Form,
  Checkbox,
  Input,
  Row,
  Col,
  Select,
  Spin,
  Button,
  notification,
} from "antd";

const Ouvidoria = ({
  form: { getFieldDecorator, validateFields, resetFields },
}) => {
  const [tipos, setTipos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingSave, setLoadingSave] = useState(false);

  useEffect(() => {
    OuvidoriaProvider.fetchTypes().then((resp) => {
      setTipos(resp);
      setLoading(false);
    });
  }, []);

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault();

      validateFields((err, values) => {
        if (!err) {
          setLoadingSave(true);

          OuvidoriaProvider.newOccurrence(values).then((resp) => {
            if (resp.done && resp.detail === "Occurence registered") {
              notification.success({
                message: "Sucesso",
                description: "Mensagem enviada!",
              });
            } else {
              notification.error({
                message: "Atenção",
                description: "Não foi possível enviar a sua mensagem!",
              });
            }
            resetFields();
            setLoadingSave(false);
          });
        }
      });
    },
    [resetFields, validateFields]
  );

  if (loading) {
    return (
      <div style={{ textAlign: "center" }}>
        <Spin />
      </div>
    );
  }

  return (
    <Form onSubmit={handleSubmit} style={{ margin: 10, padding: "0 10px" }}>
      <Row gutter={36}>
        <Col span={24} md={8}>
          <Form.Item label="Tipo de ocorrência">
            {getFieldDecorator("type_id", {
              rules: [
                { required: true, message: "Por favor selecione um tipo!" },
              ],
            })(
              <Select optionLabelProp="label" dropdownMatchSelectWidth={false}>
                {tipos.map((tipo) => (
                  <Select.Option
                    key={tipo.id}
                    value={tipo.id}
                    label={tipo.label}
                  >
                    {tipo.label} - {tipo.description}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item label="Identificação">
            {getFieldDecorator("anonymous", { initialValue: false })(
              <Checkbox>Anônimo</Checkbox>
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={24} md={12}>
          <Form.Item label="Título">
            {getFieldDecorator("title", {
              rules: [
                { required: true, message: "Por favor inclua um título!" },
              ],
            })(<Input maxLength={100} />)}
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Col span={24} md={12}>
          <Form.Item label="Mensagem">
            {getFieldDecorator("content", {
              rules: [
                { required: true, message: "Por favor inclua uma mensagem!" },
              ],
            })(<Input.TextArea />)}
          </Form.Item>
        </Col>
      </Row>
      <Form.Item>
        <Button type="primary" htmlType="submit" loading={loadingSave}>
          Enviar
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create()(Ouvidoria);
