import React from "react";
import Chart_amigos from "../Components/chart_amigos"
import Chart_faturamento from "../Components/chart_faturamento"
import Chart_faturamento2 from "../Components/chart_faturamento2"
import Chart_caixa from "../Components/chart_caixa"

//var Chart = require('chart.js');




export default () => {
  return (
    <div className="page-body custom-font page"> 
		<div class="row">
			<div class="column">
				<div className="meia-pagina-grafico">
				<Chart_faturamento name="Faturamento AdE"/>
				</div>
			</div>
			<div class="column">
				<div className="meia-pagina-grafico">
				  <Chart_amigos name="Médias gerais Simulados"/>
				</div>
			</div>
		</div>
	  </div>
  );
};  
