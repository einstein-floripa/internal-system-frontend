import React from "react";

export default () => {
  return (
    <div style={{ padding: 8, textAlign: "center" }}>
      <h1>Bem-vindo!</h1>
      <br />
      <img
        style={{
          borderRadius: "50%",
          width: "96px",
          height: "96px",
          display: "block",
          margin: "auto",
        }}
        src="https://d1fdloi71mui9q.cloudfront.net/2TQ2qKcS2WJxikWiaznA_Mv026gt9LKov0JCs"
      />
      <br></br>

      <p>
        <a target="_blank" href="https://linktr.ee/projetoeinsteinfloripa">
          LinkTree
        </a>{" "}
        |{" "}
        <a target="_blank" href="https://www.instagram.com/einsteinfloripa/">
          Instagram
        </a>
      </p>
      <br />
      <br></br>
      <h3>
        <q>
          O importante é não parar de questionar. A curiosidade tem a sua
          própria razão para existir.
        </q>
        <cite> &#8212; Albert Einstein</cite>
      </h3>
    </div>
  );
};
