import React from "react";
import { Form, Icon, Input, Button, notification, Modal } from "antd";
import themeColors from "../assets/theme/colors";
import { logo_vertical } from "../assets/images";
import { fetchNewLogin, triggerRecovery } from "../Providers/LoginProvider";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as UserActions } from "../store/ducks/user";
import { LoginProvider } from "../Providers";
import { TIPO_USUARIO } from "../utils/constantes";

const pathname = {
  [TIPO_USUARIO.ALUNO]: "/home-alunos",
  [TIPO_USUARIO.ORGANIZACAO]: "/home-organizacao",
};

class LoginPage extends React.Component {
  state = {};

  componentDidMount() {
    const { history, setDados } = this.props;

    if (!!localStorage.getItem("tokeninstein")) {
      this.setState({ loading: true });
      LoginProvider.userInfo().then((dados) => {
        if (dados) {
          setDados(dados);
          history.push({ pathname: pathname[dados.type] });
        }
        this.setState({ loading: false });
      });
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true });
        this.login(values);
      }
    });
  };

  login = async (params) => {
    const { history } = this.props;
    const json = await fetchNewLogin(params);

    if (json && json.token) {
      localStorage.setItem("tokeninstein", json.token);
      this.props.setDados(json.user_data);
      history.push({ pathname: pathname[json.user_data.type] });
    } else {
      notification.warning({
        message: "Usuário e/ou Senha inválidos.",
      });
    }
    this.setState({ loading: false });
  };

  render() {
    const { loading } = this.state;
    const { getFieldDecorator, getFieldValue, getFieldError } = this.props.form;
    const username = getFieldValue("username");
    const errorsUsername = getFieldError("username");

    return (
      <div style={{ textAlign: "center" }}>
        <div style={{ display: "inline-block" }}>
          <Form onSubmit={this.handleSubmit} style={{ maxWidth: "300px" }}>
            <div>
              <img
                width="100%"
                alt="Einsten Floripa Vestibulares"
                src={logo_vertical}
                style={{ padding: 20 }}
              />
            </div>
            <Form.Item>
              {getFieldDecorator("username", {
                rules: [
                  {
                    type: "email",
                    message: "Este não é um e-mail valido!",
                  },
                  {
                    required: true,
                    message: "Por favor insira o seu usuário!",
                  },
                ],
              })(
                <Input
                  prefix={
                    <Icon
                      type="user"
                      style={{ color: themeColors.secondary_blue }}
                    />
                  }
                  placeholder="Usuário"
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("password", {
                rules: [
                  { required: true, message: "Por favor insira a sua senha!" },
                ],
              })(
                <Input.Password
                  prefix={
                    <Icon
                      type="lock"
                      style={{ color: themeColors.secondary_blue }}
                    />
                  }
                  placeholder="Senha"
                />
              )}
            </Form.Item>
            <Form.Item>
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid*/}
              <a
                style={{ float: "right" }}
                onClick={() =>
                  Modal.confirm({
                    maskClosable: true,
                    title: "Redefinir Senha",
                    onOk: () => {
                      triggerRecovery({ email: username }).then((resp) => {
                        console.log(resp);
                        window.location = "/";
                      });
                      console.log(username);
                    },
                    okButtonProps: {
                      disabled: errorsUsername || !username,
                    },
                    okText: "Enviar",
                    content: (
                      <div>
                        {errorsUsername || !username ? (
                          <>{"Digite um email valido"}</>
                        ) : (
                          <>
                            {"Deseja redefinir a senha para o email:"}
                            <br />
                            <strong>{username}</strong>
                          </>
                        )}
                      </div>
                    ),
                  })
                }
              >
                Esqueci minha senha
              </a>
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  width: "100%",
                  backgroundColor: themeColors.primary_blue,
                }}
                loading={loading}
              >
                {"Entrar"}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(UserActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Form.create({ name: "login" })(LoginPage));
