import React, { useState, useCallback } from "react";
import { bindActionCreators } from "redux";
import { Creators as InfoAndSubsActions } from "../../store/ducks/infoAndSubs";
import { SimuladosProvider } from "../../Providers";
import ModalForm from "./ModalForm";
import { Button, notification } from "antd";
import { connect } from "react-redux";
import ListaSimulados from "../../Components/ListaSimulados";

const CadastroSimulado = ({ infoAndSubs, setInfos }) => {
  const [showModal, setShowModal] = useState(false);
  const [testEdit, setTestEdit] = useState();
  const [loadingLista, setLoadingLista] = useState(false);

  const salvar = useCallback(
    dados => {
      setLoadingLista(true);
      SimuladosProvider.salvarSimulado(dados).then(resp => {
        if (resp.detail === "test created" || resp.detail === "test modified") {
          notification.success({
            message: "Sucesso",
            description: "Simulado salvo"
          });
        } else {
          notification.error({
            message: "Erro",
            description: resp.detail || "Não foi possível salvar"
          });
        }
        SimuladosProvider.fetchInfosSubs().then(info => {
          setInfos(info);
          setLoadingLista(false);
        });
      });
    },
    [setInfos]
  );

  const apagar = useCallback(
    id => {
      setLoadingLista(true);
      setShowModal(false);
      SimuladosProvider.apagarSimulado(id).then(resp => {
        console.log('resp: ', resp)
        if (resp.detail === "test deleted") {
          notification.success({
            message: "Sucesso",
            description: "Simulado removido"
          });
        } else {
          notification.error({
            message: "Erro",
            description: resp.detail || "Não foi possível remover"
          });
        }
        SimuladosProvider.fetchInfosSubs().then(info => {
          setInfos(info);
          setLoadingLista(false);
        });
      });
    },
    [setInfos]
  );

  const handleClickNovoSimulado = useCallback(() => {
    setTestEdit(undefined);
    setShowModal(true);
  }, []);

  const handleClickSimulado = useCallback(test => {
    setTestEdit(test);
    setShowModal(true);
  }, []);

  return (
    <div style={{ padding: 12 }}>
      {showModal && (
        <ModalForm
          setShowModal={setShowModal}
          test={testEdit}
          salvar={salvar}
          apagar={apagar}
        />
      )}
      <Button type="primary" onClick={handleClickNovoSimulado}>
        Novo Simulado
      </Button>

      <div style={{ marginTop: 10 }}>
        <ListaSimulados
          loading={loadingLista}
          infoAndSubs={infoAndSubs}
          onClick={handleClickSimulado}
        ></ListaSimulados>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  infoAndSubs: state.infoAndSubs
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(InfoAndSubsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CadastroSimulado);
