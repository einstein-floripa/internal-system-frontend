import React, { useCallback } from "react";
import moment from "moment";
import {
  Form,
  Input,
  Modal,
  Button,
  Checkbox,
  DatePicker,
  TimePicker,
  Row,
  Col,
  Popconfirm
} from "antd";

const ModalForm = ({
  form: { getFieldDecorator, validateFields },
  setShowModal,
  test = {},
  salvar,
  apagar,
}) => {
  const isEdit = !!test.id;
  const handleSubmit = useCallback(
    e => {
      e.preventDefault();
      validateFields((err, values) => {
        if (!err) {
          salvar({
            open: values.open,
            label: values.label,
            open_sub:
              values.dia_abertura.format("YYYY-MM-DD") +
              " " +
              values.hora_abertura.format("HH:mm:ss"),
            close_sub:
              values.dia_fechamento.format("YYYY-MM-DD") +
              " " +
              values.hora_fechamento.format("HH:mm:ss"),
            test_datetime:
              values.dia_prova.format("YYYY-MM-DD") +
              " " +
              values.hora_prova.format("HH:mm:ss"),
            id: test.id
          });
          setShowModal(false);
        }
      });
    },
    [setShowModal, test.id, validateFields, salvar]
  );

  return (
    <Modal
      title={`${isEdit ? "Editar" : "Novo"} Simulado`}
      visible
      footer={null}
      closable={false}
    >
      <Form onSubmit={handleSubmit}>
        <Row gutter={12}>
          <Col span={18}>
            <Form.Item label="Nome">
              {getFieldDecorator("label", {
                initialValue: test.label,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira um nome para o simulado"
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="Status" style={{ float: "right" }}>
              {getFieldDecorator("open", {
                valuePropName: "checked",
                initialValue: isEdit ? test.open : true
              })(<Checkbox>Automatico</Checkbox>)}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col span={12}>
            <Form.Item label="Dia de abertura das inscrições">
              {getFieldDecorator("dia_abertura", {
                initialValue: moment(test.open_sub),
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira o momento para a abertura das inscrições do simulado"
                  }
                ]
              })(<DatePicker format={"DD/MM/YYYY"} />)}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Hora de abertura das inscrições">
              {getFieldDecorator("hora_abertura", {
                initialValue: moment(test.open_sub),
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira a hora para a abertura das inscrições do simulado"
                  }
                ]
              })(<TimePicker />)}
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={12}>
          <Col span={12}>
            <Form.Item label="Dia de fechamento das inscrições">
              {getFieldDecorator("dia_fechamento", {
                initialValue: moment(test.close_sub),
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira o dia para o fechamento das inscrições do simulado"
                  }
                ]
              })(<DatePicker format={"DD/MM/YYYY"} />)}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Hora de fechamento das inscrições">
              {getFieldDecorator("hora_fechamento", {
                initialValue: moment(test.close_sub),
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira a hora para o fechamento das inscrições do simulado"
                  }
                ]
              })(<TimePicker />)}
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={12}>
          <Col span={12}>
            <Form.Item label="Dia da prova">
              {getFieldDecorator("dia_prova", {
                initialValue: moment(test.test_datetime),
                rules: [
                  {
                    required: true,
                    message: "Por favor insira o dia da realização do simulado"
                  }
                ]
              })(<DatePicker format={"DD/MM/YYYY"} />)}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Hora da prova">
              {getFieldDecorator("hora_prova", {
                initialValue: moment(test.test_datetime),
                rules: [
                  {
                    required: true,
                    message: "Por favor insira a hora da realização do simulado"
                  }
                ]
              })(<TimePicker />)}
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            {isEdit && (
              <Form.Item style={{ float: "left" }}>
                <Popconfirm
                  title={<>
                  <p>Você tem certeza que deseja apagar este simulado?</p>
                  <p>Todas as inscrições realizadas até o momento serão perdidas!</p>
                  </>}
                  okText="Sim"
                  okType="danger"
                  cancelText="Cancelar"
                  onConfirm={() => apagar(test.id)}
                >
                  <Button type="danger">Apagar</Button>
                </Popconfirm>
              </Form.Item>
            )}
            <Form.Item style={{ float: "right" }}>
              <Button onClick={() => setShowModal(false)}>Cancelar</Button>
              <Button
                htmlType="submit"
                type="primary"
                style={{ marginLeft: 12 }}
              >
                Salvar
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default Form.create()(ModalForm);
