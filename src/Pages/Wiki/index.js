import React from "react";
import Modal from "react-modal";

const customStyles = {
  content: {
    display: "flex",
    flexDirection: "column",
    flexWrap: "nowrap",
    alignContent: "center",
    alignItems: "center",
    justifyContent: "flex-start",
  },
};

export default () => {
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function opentModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <div style={{ padding: "8px" }}>
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Processo XYZ</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>

        <iframe
          style={{ width: "980px", height: "600px" }}
          src="https://docs.google.com/document/d/e/2PACX-1vR-iBE8JO8oYVN9GkP6LCB6awqMX-4U1HM70YJzHZHluvQ6J6v8Pwg5tRjtYJV8dKavMXRMjkYLVdtA/pub?embedded=true"
        ></iframe>
        <br />
        <button style={{ cursor: "pointer" }} onClick={closeModal}>
          Fechar
        </button>
      </Modal>
      <span style={{ fontSize: "32px" }}>Vale do Silício</span>
      <br />
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
      velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
      cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
      est laborum. <br />
      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
      doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
      Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
      fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
      sequi nesciunt. <br />
      Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
      consectetur, adipisci velit, sed quia non numquam eius modi tempora
      incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad
      minima veniam, quis nostrum exercitationem ullam corporis suscipit
      laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum
      iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae
      consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
      <hr />
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          alignContent: "center",
          justifyContent: "space-evenly",
          flexDirection: "row",
        }}
      >
        <div style={{ padding: "8px" }}>
          <img src="https://placedog.net/150/150?random" />
          <br />
          Fulano
          <br />
          Lorem ipsum dolor sit amet
        </div>
        <div style={{ padding: "8px" }}>
          <img src="https://placedog.net/149/150?random" />
          <br />
          Ciclano
          <br />
          Lorem ipsum dolor sit amet
        </div>
        <div style={{ padding: "8px" }}>
          <img src="https://placedog.net/151/150?random" />
          <br />
          Beltrano
          <br />
          Lorem ipsum dolor sit amet
        </div>
      </div>
      <hr />
      <div>
        <span>Pesquisar:</span>
        <input
          maxlength="100"
          type="text"
          id="title"
          class="ant-input"
          style={{ margin: "10px", width: "auto" }}
        />

        <label>Categoria: </label>
        <select name="categ" id="cars">
          <option value="volvo">Todos</option>
          <option value="Teste1">Teste1</option>
          <option value="Teste2">Teste2</option>
          <option value="Teste3">Teste3</option>
        </select>
      </div>
      <div
        style={{
          display: "flex",
          flexFlow: "row nowrap",
          alignContent: "flex-start",
          alignItems: "flex-start",
          justifyContent: "center",
          flexWrap: "wrap",
        }}
      >
        <div className="wrap">
          <div style={{ width: "100%" }}>
            <span>Processo XYZ</span>
            <span
              style={{
                float: "right",
                backgroundColor: "aquamarine",
                padding: "2px 1em",
                borderRadius: "6px",
              }}
            >
              Teste1
            </span>
          </div>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <hr />
          <span>
            <a onClick={opentModal}>Abrir</a> | <a>Compartilhar</a> |{" "}
            <a>Editar</a>
          </span>
        </div>
        <div className="wrap">
          <div style={{ width: "100%" }}>
            <span>Processo XYZ</span>
            <span
              style={{
                float: "right",
                backgroundColor: "aquamarine",
                padding: "2px 1em",
                borderRadius: "6px",
              }}
            >
              Teste1
            </span>
          </div>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <hr />
          <span>
            <a>Abrir</a> | <a>Compartilhar</a> | <a>Editar</a>
          </span>
        </div>
        <div className="wrap">
          <div style={{ width: "100%" }}>
            <span>Processo XYZ</span>
            <span
              style={{
                float: "right",
                backgroundColor: "cadetblue",
                padding: "2px 1em",
                borderRadius: "6px",
              }}
            >
              Teste2
            </span>
          </div>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <hr />
          <span>
            <a>Abrir</a> | <a>Compartilhar</a> | <a>Editar</a>
          </span>
        </div>
        <div className="wrap">
          <div style={{ width: "100%" }}>
            <span>Processo XYZ</span>
            <span
              style={{
                float: "right",
                backgroundColor: "burlywood",
                padding: "2px 1em",
                borderRadius: "6px",
              }}
            >
              Teste3
            </span>
          </div>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <hr />
          <span>
            <a>Abrir</a> | <a>Compartilhar</a> | <a>Editar</a>
          </span>
        </div>
        <div className="wrap">
          <div style={{ width: "100%" }}>
            <span>Processo XYZ</span>
            <span
              style={{
                float: "right",
                backgroundColor: "aquamarine",
                padding: "2px 1em",
                borderRadius: "6px",
              }}
            >
              Teste1
            </span>
          </div>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <hr />
          <span>
            <a>Abrir</a> | <a>Compartilhar</a> | <a>Editar</a>
          </span>
        </div>
      </div>
      <button
        style={{ cursor: "pointer", display: "block", marginLeft: "auto" }}
      >
        + Adicionar
      </button>
    </div>
  );
};
