import BaseProvider from "./BaseProviderDash";

export const fetchAmigosDoEinstein = values => {
  const url = "/times/doadores-mes";
  return BaseProvider.GET(url);
};
