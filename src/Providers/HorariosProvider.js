import BaseProvider from "./BaseProvider";

/**
 */
export const fetchTurmas = turma =>
  BaseProvider.GET(`/internal/schedule/${turma ? turma : ""}`);

export const fetchDisciplinas = () =>
  BaseProvider.GET("/internal/schedule/subjects/");

export const salvarHorarios = (turma, horarios) =>
  BaseProvider.POST(`internal/schedule/${turma}`, horarios);
