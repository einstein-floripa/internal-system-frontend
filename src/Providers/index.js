import * as SimuladosProvider from "./SimuladosProvider";
import * as AlunosProvider from "./AlunosProvider";
import * as ResourcesProvider from "./ResourcesProvider";
import * as LoginProvider from "./LoginProvider";
import * as ContatosProvider from "./ContatosProvider";
import * as HorariosProvider from "./HorariosProvider";
import * as ChamadaProvider from "./ChamadaProvider";
import * as OuvidoriaProvider from "./OuvidoriaProvider";
import * as AmigosEinsteinProvider from "./AmigosEinsteinProvider";

export {
  SimuladosProvider,
  AlunosProvider,
  ResourcesProvider,
  LoginProvider,
  ContatosProvider,
  HorariosProvider,
  ChamadaProvider,
  OuvidoriaProvider,
  AmigosEinsteinProvider
};
