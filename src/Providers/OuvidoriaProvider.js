import BaseProvider from "./BaseProvider";

/**
 * 
 */
export const fetchTypes = () => BaseProvider.GET("/internal/occurrences/types/");

/**
 * 
 */
export const newOccurrence = (params) => BaseProvider.POST("/internal/occurrences/create/", params);

/**
 * 
 */
export const fetchSolveds = (num = 10) => BaseProvider.GET(`/internal/occurrences/solveds?num_occur=${num}`);

/**
 * 
 */
export const fetchUnsolveds = () => BaseProvider.GET("/internal/occurrences/unsolveds/");

/**
 * 
 */
export const toggleResolved = (id) => BaseProvider.POST('/internal/occurrences/', {
    id, action: "toggle-solved"
});
