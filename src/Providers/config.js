const proxy_url = 'https://cors-anywhere.herokuapp.com/';
export const IP_BACKEND = proxy_url + 'https://internal-system-backend.herokuapp.com';
export const IP_DASH_BACKEND =  'https://dev-dashboard-flask-backend.herokuapp.com';

export default () => ({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    Authorization: localStorage.getItem("tokeninstein")
      ? `Token ${localStorage.getItem("tokeninstein")}`
      : undefined
  }
});
