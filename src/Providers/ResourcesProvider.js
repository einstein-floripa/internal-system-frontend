import BaseProvider from "./BaseProvider";

/**
 * Retorna uma lista com todos os recursos extras a serem disponibilizados para o usuário
 */
export const fetchResources = () => BaseProvider.GET("/internal/resources");
