import BaseProvider from "./BaseProvider";

/**
 * Retorna todas as informações sobre as inscrições e parametros para cadastro de uma inscrição
 */
export const fetchInfosSubs = () => {
  const url = "/internal/tests/info-and-subs";
  return BaseProvider.GET(url);
};

/**
 * Recebe como parametro um objeto contendo as informações da inscricao
 * @param {object} body objeto com parametros selecionados: {id_test, id_course, id_lang, id_quota}
 */
export const registrarInscricao = body => {
  const url = "/internal/tests/info-and-subs";
  return BaseProvider.POST(url, body);
};

/**
 * Recebe como parametro o código da prova a ser cancelada a inscricao
 * @param {int} id_test codigo de referencia da prova
 */
export const cancelarInscricao = id_test => {
  const url = `/internal/tests/info-and-subs`;
  return BaseProvider.DELETE(url, { id_test });
};

export const salvarSimulado = dados =>
  BaseProvider.POST("/internal/tests/create-and-modify/", dados);

export const apagarSimulado = id =>
  BaseProvider.DELETE("/internal/tests/create-and-modify/", { id });
