const proxy_url = 'https://cors-anywhere.herokuapp.com/';
export const IP_DASH_BACKEND =  'https://dev-dashboard-flask-backend.herokuapp.com';

export default () => ({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: {
      'username':'einst_301', 
      'password':'M3LH0R_D3P4RT4M3NT0'
    }
  }
});
