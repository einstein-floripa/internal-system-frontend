import BaseProvider from "./BaseProvider";
import moment from "moment";

/**
 * Retorna o PDF do atestado de matrícula
 */
export const fetchCertificado = () =>
  BaseProvider.BLOB("/internal/certification");

export const persistRegistro = values => {
  const url = "/attendance/register/";
  const params = {
    code: values.matricula,
    action_timestamp:
      values.data && values.hora
        ? `${values.data.format("YYYY-MM-DD")} ${values.hora.format(
            "HH:mm:ss"
          )}`
        : moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"),
    id_state: values.estado
  };

  return BaseProvider.POST(url, params);
};

export const fetchAluno = values => {
  const url = `/attendance/search/${values}`;
  return BaseProvider.GET(url);
};
