import BaseProvider from "./BaseProvider";

/**
 * Recebe como parametro um objeto com informações de login e senha, retorna o token de autenticação
 * @param {object} params parametros do login: {username, password}
 */
export const fetchNewLogin = params =>
  BaseProvider.POST("/internal/api-token-auth/", params);

/**
 * Recebe como parametro um objeto com informações do usuário a ser criado
 * @param {object} params parametros de criação do usuário: {user_name, cpf, password}
 */
export const persistUser = params =>
  BaseProvider.POST("/apichamada/usuario", params);

/**
 * Recebe como parametro um objeto contendo o email do usuario a ser disparado o evento de recovery de senha
 * @param {object} params parametros de disparo do recovery: {email}
 */
export const triggerRecovery = params =>
  BaseProvider.POST("/internal/recovery/trigger", params);

/**
 * Recebe como parametro um objeto contendo o token de recovery junto com a  nova senha
 * @param {object} params parametros para a troca da senha: {token, password}
 */
export const recoveryPassword = params =>
  BaseProvider.POST("/internal/recovery/reset", params);

/**
 * Retorna as informações do usuário a partir do token de autenticação 
 */
export const userInfo = () =>
  BaseProvider.GET("/internal/user-info/");