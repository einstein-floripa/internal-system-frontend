import config, { IP_DASH_BACKEND } from "./configDash";


const BLOB = async url => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, config());
    return await response.blob();
  } catch (err) {
    throw new Error(`Erro na chamada BLOB: ${url}`);
  }
};

const GET = async url => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, {
      ...config(),
      referrer: "no-referrer"
    });
    if (response.status === 401) {
      window.location = "/"; // alterar depois para history.push
      return {};
    } else {
      const json = response.status !== 204 ? await response.json() : {};
      return json;
    }
  } catch (err) {
    console.log("err: ", err);
    //throw new Error(`Erro na chamada GET: ${url}`);
  }
};

const POST = async (url, body, data) => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, {
      ...config(),
      method: "POST",
      body: data || JSON.stringify(body),
      referrer: "no-referrer"
    });
    try {
      return await response.json();
    } catch (err) {
      console.log(err)
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada POST: ${url}`);
  }
};

const PUT = async (url, body) => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, {
      ...config(),
      method: "PUT",
      body: JSON.stringify(body)
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada PUT: ${url}`);
  }
};

const DELETE = async (url, body) => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, {
      ...config(),
      method: "DELETE",
      body: JSON.stringify(body)
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada DELETE: ${url}`);
  }
};

const UPLOAD = async (url, body) => {
  try {
    const response = await fetch(process.env.NODE_ENV === 'production' ? url : `${IP_DASH_BACKEND}${url}`, {
      headers: {
        //tokenHash: config().headers.tokenHash,
      },
      method: "POST",
      body
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada UPLOAD: ${url}`);
  }
};

export default {
  GET,
  POST,
  PUT,
  DELETE,
  UPLOAD,
  BLOB
};
