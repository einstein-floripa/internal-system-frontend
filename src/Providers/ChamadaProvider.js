import BaseProvider from "./BaseProvider";

/**
 *
 */
export const fetchDadosChamada = month =>
  BaseProvider.GET(`/attendance/table/${month}`);

export const getEstados = values => {
  const url = "/attendance/states/";
  return BaseProvider.GET(url);
};
