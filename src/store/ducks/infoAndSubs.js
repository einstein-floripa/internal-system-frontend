import { createActions, createReducer } from "reduxsauce";

export const { Types, Creators } = createActions({
  setInfos: ["json"]
});

const INITIAL_STATE = { courses: [], langs: [], quotas: [], tests: [] };

const setInfos = (state = INITIAL_STATE, action) => ({
  ...state,
  ...action.json
});

export default createReducer(INITIAL_STATE, {
  [Types.SET_INFOS]: setInfos
});
