import { createActions, createReducer } from "reduxsauce";

export const { Types, Creators } = createActions({
  setDados: ["json"]
});

const INITIAL_STATE = {};

const setDados = (state = INITIAL_STATE, action) => ({
  ...state,
  ...action.json
});

export default createReducer(INITIAL_STATE, {
  [Types.SET_DADOS]: setDados
});
