import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import todos from "./todos";
import infoAndSubs from "./infoAndSubs";
import user from "./user";

export default history =>
  combineReducers({ router: connectRouter(history), todos, infoAndSubs, user });
