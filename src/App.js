import React from "react";
import Routes from "./routes";
import { Provider } from "react-redux";
import store from "./store";
import "./App.css";
import "antd/dist/antd.css";
import { LocaleProvider } from "antd";
import ptBR from "antd/es/locale-provider/pt_BR";

const App = () => (
  <LocaleProvider locale={ptBR}>
    <Provider store={store}>
      <Routes />
    </Provider>
  </LocaleProvider>
);

export default App;
