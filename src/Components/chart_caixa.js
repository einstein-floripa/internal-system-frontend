import React from 'react';
import Bar from 'react-chartjs-2';

/*
function get_req(){
  
  const url = 'http://127.0.0.1:5000/times/att-amigos-einstein';

  fetch(url)
  .then((resp) => resp.json())
  .then(function(data) {
  let authors = data.data;
  return authors}

  };
*/
class Chart_caixa extends React.Component {

  constructor(props){
		super(props);
		this.state = {
      data:{
        labels: ['January', 'February', 'March',
        'April', 'May','june'],
          datasets: [
            {
              label: this.props.name,
              fill: false,
              lineTension: 0.25,
              backgroundColor: 'rgba(75,192,192,0.4)',
              borderColor: 'rgba(75,192,192,1)',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: 'rgba(75,192,192,1)',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: 'rgba(220,220,220,1)',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointHoverBorderWidth: 2,
              pointRadius: 4,
              pointHitRadius: 10,
              data: [51.7, 36.63]
            }
              ]       
            },
      chart_option: {
        title:{
          display:true,
          text: this.props.name,
          fontSize:20
        },
        legend:{
          display:true,
          position:'right'
        }}
                };
  };
  
  compononentDidMount(){

  };

 
  
  render() {
    return (
      <div ClassName="chart-dash">
        <Bar
          data = {this.state.data}
          options = {this.state.chart_option}
        />
      </div>
    );
  }
};


export default Chart_caixa;
