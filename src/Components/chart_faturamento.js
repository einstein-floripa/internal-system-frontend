import React from 'react';
import {Line,Bar} from 'react-chartjs-2';

/*
function get_req(){
  
  const url = 'http://127.0.0.1:5000/times/att-amigos-einstein';

  fetch(url)
  .then((resp) => resp.json())
  .then(function(data) {
  let authors = data.data;
  return authors}

  };
*/





class Chart_faturamento extends React.Component {

  constructor(props){
		super(props);
		this.state = {
      data:{
        labels: [
          '2020-01', '2020-02', '2020-03', '2020-04', '2020-05', '2020-06', 
          '2020-07', '2020-08', '2020-09','2020-10', '2020-11', '2020-12',
        ],
          datasets: [
            {
              label: this.props.name,
              fill: false,
              lineTension: 0.25,
              backgroundColor: 'rgba(75,192,192,0.4)',
              borderColor: 'rgba(75,192,192,1)',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: 'rgba(75,192,192,1)',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: 'rgba(220,220,220,1)',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointHoverBorderWidth: 2,
              pointRadius: 4,
              pointHitRadius: 10,
              data: [126, 103, 117, 131, 491, 692, 979, 889, 828, 360]
            }
              ]       
            },
      chart_option: {
        title:{
          display:true,
          text: this.props.name,
          fontSize:20
        },
        legend:{
          display:true,
          position:'right'
        }}
                };
  };
  
  compononentDidMount(){

  };

 
  
  render() {
    return (
      <div ClassName="chart-dash">
        <Line
          data = {this.state.data}
          options = {this.state.chart_option}
        />
      </div>
    );
  }
};


export default Chart_faturamento;
