import React from 'react';
import Bar from 'react-chartjs-2';
import 'axios';
import { AmigosEinsteinProvider } from "../Providers";
const axios = require('axios');
class Chart_amigos extends React.Component {

  constructor(props){
		super(props);
		this.state = {
      data:{
        labels:[]   ,
        datasets: [
            {
              label: this.props.name,
              backgroundColor: 'rgba(75,192,192,1)',
              data: [65, 59, 80, 81, 56, 10, 11, 54, 12, 45],
              fill: false
            }
              ]       
            },
      chart_option: {
        title:{
          display:true,
          text: this.props.name,
          fontSize:20
        },
        legend:{
          display:true,  
          position:'right'
        }}
                }
  };
  
  componentDidMount(){
    /*
    AmigosEinsteinProvider.fetchAmigosDoEinstein().then(resp => {
    
    const responseDates = resp;
    console.log(responseDates);
    
    })

  };*/

  const proxy_url = 'https://cors-anywhere.herokuapp.com/';

    
    axios.get(proxy_url+`https://dev-dashboard-flask-backend.herokuapp.com/times/doadores-mes`, {
        headers : {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*"
        },
        auth: {
          'username':'einst_301', 
          'password':'M3LH0R_D3P4RT4M3NT0'
           }
         
      
    }).then(function(response) {
      console.log('Authenticated');
      const api = response.data;
      this.state.data.labels = api.Year_month
      console.log(api)
    }).catch(function(error) {
      console.log('Error on Authentication');
      console.log(error);
    });
      
  };

 
  
  render() {
    return (
      <div ClassName="chart-dash">
        <Bar
          data = {this.state.data}
          options = {this.state.chart_option}
        />
      </div>
    )
  };

}

export default Chart_amigos;
