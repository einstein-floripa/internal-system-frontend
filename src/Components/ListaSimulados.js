import React from "react";
import { isOpen } from "../utils/functions";
import moment from "moment";
import { Card, Spin } from "antd";

export default ({ infoAndSubs, onClick, loading }) => {
  if (loading) {
    return (
      <div style={{ textAlign: "center" }}>
        <Spin />
      </div>
    );
  }

  return infoAndSubs.tests.map(t => {
    let title;
    if (t.user_sub) {
      title = (
        <>
          {t.label} - <span style={{ color: "green" }}>{` INSCRITO`}</span>
        </>
      );
    } else if (isOpen(t)) {
      title = (
        <>
          {t.label} - <span style={{ color: "blue" }}>{` ABERTO`}</span>
        </>
      );
    } else {
      title = (
        <>
          {t.label} - <span style={{ color: "red" }}>{` FECHADO`}</span>
        </>
      );
    }

    return (
      <Card
        key={t.id}
        onClick={() => onClick(t)}
        title={title}
        disabled={!t.open}
        hoverable
      >
        <span>{`Data de abertura: ${moment(t.open_sub).format(
          "DD/MM/YYYY HH:mm:ss"
        )}`}</span>
        <br />
        <span>{`Data de fechamento: ${moment(t.close_sub).format(
          "DD/MM/YYYY HH:mm:ss"
        )}`}</span>
        <br />
        <span>{`Data da prova: ${moment(t.test_datetime).format(
          "DD/MM/YYYY HH:mm:ss"
        )}`}</span>
      </Card>
    );
  });
};
