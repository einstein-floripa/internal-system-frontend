const proxy = require("http-proxy-middleware");

module.exports = function (app) {
  // ...
  app.use(
    proxy("/internal", {
      target: "http://internal-system-backend.herokuapp.com/",
      changeOrigin: true
    }),
    proxy("/attendance", {
      target: "http://internal-system-backend.herokuapp.com/",
      changeOrigin: true
    })
  );
};
